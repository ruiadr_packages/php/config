<?php

namespace Ruiadr\Config;

use Ruiadr\Base\Common\Parameters;
use Ruiadr\Cache\Base\Interface\CacheBaseInterface;
use Ruiadr\Config\Exception\ConfigException;
use Ruiadr\Config\Interface\ConfigCacheInterface;
use Ruiadr\Config\Interface\ConfigInterface;
use Ruiadr\Utils\ArrayUtils;
use Symfony\Component\Yaml\Yaml;

class Config extends Parameters implements ConfigInterface
{
    private ?array $yml = null;

    /**
     * Construction de l'objet de configuration.
     * Lève une exception de type ConfigException si une erreur survient.
     *
     * @param string                $configFile  Fichier de configuration
     * @param ?string               $env         Environnement ciblé
     * @param ?ConfigCacheInterface $configCache Interface dédiée au stockage
     *                                           de la configuration en cache
     *
     * @throws ConfigException
     */
    public function __construct(
        private readonly string $configFile,
        private readonly ?string $env = null,
        private readonly ?ConfigCacheInterface $configCache = null
    ) {
        // Ici on ne souhaite pas activer le cast auto pour récupérer
        // les valeurs brutes stockées dans le fichier de configuration.
        parent::__construct($this->buildDataFromYML(), false);
    }

    final public function getConfigFile(): string
    {
        return $this->configFile;
    }

    final public function getEnv(): ?string
    {
        return $this->env;
    }

    final public function getConfigCache(): ?ConfigCacheInterface
    {
        return $this->configCache;
    }

    /**
     * Permet de déterminer si une instance de cache a été passée au
     * constructeur de l'objet courant.
     *
     * @return bool true lorsqu'une instance de cache a été passée au constructeur,
     *              false dans le cas contraire
     */
    private function hasConfigCache(): bool
    {
        return $this->configCache instanceof ConfigCacheInterface;
    }

    final public function getCacheInterface(): ?CacheBaseInterface
    {
        return $this->hasConfigCache() ? $this->configCache->getCache() : null;
    }

    /**
     * Récupère la configuration stockée en cache, ou "null" si elle ne peut
     * être récupérée. Soit aucune instance de cache n'a été passée au
     * constructeur, soit la clé ne s'y trouve pas.
     *
     * @return ?array Le tableau récupéré du cache, "null" sinon
     */
    private function getFromCache(): ?array
    {
        return $this->hasConfigCache() ? $this->configCache->getFromCache() : null;
    }

    /**
     * Stocker dans le cache le tableau $data lorsqu'une instance
     * de cache a été passée lors de la construction de l'objet.
     *
     * @param array $data Le tableau à mettre en cache
     */
    private function addToCache(array $data): void
    {
        if ($this->hasConfigCache()) {
            $this->configCache->addToCache($data);
        }
    }

    /**
     * Extrait de $sourceConfig passé en paramètre la section de configuration
     * de l'environnement $env passé au constructeur.
     *
     * Lorsque la section existe, elle est retournée par la méthode et est retirée de $sourceConfig.
     * Dans le cas contraire, le tableau $sourceConfig n'est pas modifié mais la méthode
     * retourne "null" cette fois-ci.
     *
     * Si $sourceConfig vaut :
     * [
     *    'test1' => 'test1',
     *    'test2' => 'test2',
     *    'env' => [
     *        'dev' => [
     *            'test2' => 'test2 de dev'
     *        ]
     *    ]
     * ]
     *
     * Avec $env à 'dev', la méthode retourne :
     * [
     *    'test2' => 'test2 de dev',
     * ]
     * + la clé 'env' est retirée de $sourceConfig.
     *
     * Avec $env à 'prod', la méthode retourne "null", le tableau $sourceConfig n'est pas modifié.
     *
     * @param array &$sourceConfig Tableau source
     *
     * @return ?array Résultat de l'extraction, "null" lorsque aucune extraction
     *                ne peut être réalisée
     */
    private function extractCurrentEnvConfig(array &$sourceConfig): ?array
    {
        $envConfig = null;

        if (array_key_exists(ConfigInterface::CONFIG_ENV, $sourceConfig)) {
            if (isset($sourceConfig[ConfigInterface::CONFIG_ENV][$this->env])) {
                $envConfig = $sourceConfig[ConfigInterface::CONFIG_ENV][$this->env];
            }
            unset($sourceConfig[ConfigInterface::CONFIG_ENV]);
        }

        return $envConfig;
    }

    /**
     * Retourne le tableau final construit à partir des éléments qui se
     * trouvent dans le fichier YML de configuration.
     *
     * Lève une exception de type ConfigException si une erreur survient.
     *
     * Le settings suivant :
     *     base:                                <- Configuration de base
     *       test1: "Mon test1"
     *       test2: "Mon test2"
     *       env:                               <- Configuration de base par environnement
     *         dev:
     *           test1: "Mon test1 de dev"
     *     entree_1:                            <- Configuration ciblée 1
     *       test1: "Le test1 de mon entree1"
     *     entree_2:                            <- Configuration ciblée 2
     *       test2: "Le test2 de mon entree2"
     *       env:                               <- Configuration ciblée 2 par environnement
     *         dev:
     *           test2: "Mon test2 de dev"
     *
     * Va donner après le merge sans ciblage d'environnement :
     *     entree_1:
     *       test1: "Le test1 de mon entree1"
     *       test2: "Mon test2"
     *     entree_2:
     *       test1: "Mon test1"
     *       test2: "Le test2 de mon entree2"
     *
     * Va donner après le merge avec un ciblage sur l'environnement dev :
     *     entree_1:
     *       test1: "Le test1 de mon entree1"
     *       test2: "Mon test2"
     *     entree_2:
     *       test1: "Mon test1 de dev"
     *       test2: "Mon test2 de dev"
     *
     * @return array Le tableau final
     *
     * @throws ConfigException
     */
    private function buildDataFromYML(): array
    {
        $this->yml = $this->getFromCache();
        if (null !== $this->yml) {
            return $this->yml;
        }

        $this->yml = [];

        try {
            $this->yml = Yaml::parseFile($this->configFile);

            if (array_key_exists(ConfigInterface::CONFIG_BASE, $this->yml)) {
                // Configuration de base.
                $baseConfig = $this->yml[ConfigInterface::CONFIG_BASE];
                unset($this->yml[ConfigInterface::CONFIG_BASE]);

                // Configuration de base par environnement.
                $envBaseConfig = $this->extractCurrentEnvConfig($baseConfig);
                if (is_array($envBaseConfig)) {
                    $baseConfig = ArrayUtils::deepMerge($baseConfig, $envBaseConfig);
                }

                foreach ($this->yml as $key => $value) {
                    // Configuration ciblée.
                    $this->yml[$key] = ArrayUtils::deepMerge($baseConfig, $value);

                    // Configuration ciblée par environnement.
                    $envTargetedConfig = $this->extractCurrentEnvConfig($this->yml[$key]);
                    if (is_array($envTargetedConfig)) {
                        $this->yml[$key] = ArrayUtils::deepMerge($this->yml[$key], $envTargetedConfig);
                    }
                }
            }

            $this->addToCache($this->yml);
        } catch (\Exception $e) {
            throw new ConfigException($e);
        }

        return $this->yml;
    }
}
