<?php

namespace Ruiadr\Config\Interface;

use Ruiadr\Base\Common\Interface\ParametersInterface;
use Ruiadr\Cache\Base\Interface\CacheBaseInterface;

interface ConfigInterface extends ParametersInterface
{
    final public const CONFIG_BASE = 'base';
    final public const CONFIG_ENV = 'env';

    /**
     * Retourne le chemin du fichier de configuration qui a servi
     * à la construction de l'objet.
     *
     * @return string Le chemin du fichier de configuration
     */
    public function getConfigFile(): string;

    /**
     * Retourne la valeur de l'environnement ciblé lors de la
     * construction de l'objet.
     *
     * @return ?string L'environnement ciblé
     */
    public function getEnv(): ?string;

    /**
     * Retourne l'instance de ConfigCacheInterface lorsqu'elle est passée
     * au constructeur de l'objet courant.
     *
     * @return ?ConfigCacheInterface Instance de ConfigCacheInterface ou null
     */
    public function getConfigCache(): ?ConfigCacheInterface;

    /**
     * Retourne l'instance de cache qui a été initialisée lors de la
     * construction de l'objet courant.
     *
     * @return ?CacheBaseInterface L'objet de cache lorsqu'il est instancié
     */
    public function getCacheInterface(): ?CacheBaseInterface;
}
