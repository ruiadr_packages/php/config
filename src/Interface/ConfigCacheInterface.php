<?php

namespace Ruiadr\Config\Interface;

use Ruiadr\Cache\Base\Interface\CacheBaseInterface;

interface ConfigCacheInterface
{
    final public const DEFAULT_CACHE_KEY = 'config';
    final public const DEFAULT_CACHE_TTL = 86400;

    /**
     * Retourne l'interface de cache qui a servi à la construction
     * de l'objet.
     *
     * @return CacheBaseInterface Interface de cache
     */
    public function getCache(): CacheBaseInterface;

    /**
     * Ajouter un tableau de configuration en cache.
     *
     * @param array $data Tableau de configuration
     */
    public function addToCache(array $data): void;

    /**
     * Récupérer le tableau de configuration qui a été stocké en cache.
     * Retourne "null" si aucune donnée n'a été trouvée.
     *
     * @return ?array Tableau contenant les données stockées en cache,
     *                "null" si rien n'a été trouvé
     */
    public function getFromCache(): ?array;
}
