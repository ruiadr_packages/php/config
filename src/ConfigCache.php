<?php

namespace Ruiadr\Config;

use Ruiadr\Cache\Base\Interface\CacheBaseInterface;
use Ruiadr\Config\Interface\ConfigCacheInterface;

final class ConfigCache implements ConfigCacheInterface
{
    /**
     * @param CacheBaseInterface $cache    Instance de cache
     * @param string             $cacheKey Clé pour stocker la configuration en cache,
     *                                     par défaut : ConfigCacheInterface::DEFAULT_CACHE_KEY
     * @param int                $cacheTTL Durée de vie du cache
     *                                     par défaut : ConfigCacheInterface::DEFAULT_CACHE_TTL
     */
    public function __construct(
        private readonly CacheBaseInterface $cache,
        private readonly string $cacheKey = ConfigCacheInterface::DEFAULT_CACHE_KEY,
        private readonly int $cacheTTL = ConfigCacheInterface::DEFAULT_CACHE_TTL
    ) {
    }

    final public function getCache(): CacheBaseInterface
    {
        return $this->cache;
    }

    final public function addToCache(array $data): void
    {
        $this->cache->set($this->cacheKey, serialize($data), $this->cacheTTL);
    }

    final public function getFromCache(): ?array
    {
        return $this->cache->exists($this->cacheKey)
            ? unserialize($this->cache->get($this->cacheKey))
            : null;
    }
}
