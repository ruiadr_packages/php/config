<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Base\Common\Interface\ParametersInterface;
use Ruiadr\Cache\Base\Interface\CacheBaseInterface;
use Ruiadr\Cache\FileCache;
use Ruiadr\Cache\Interface\FileCacheInterface;
use Ruiadr\Cache\Interface\RedisCacheInterface;
use Ruiadr\Cache\RedisCache;
use Ruiadr\Config\Config;
use Ruiadr\Config\ConfigCache;
use Ruiadr\Config\Exception\ConfigException;
use Ruiadr\Config\Interface\ConfigCacheInterface;
use Ruiadr\Config\Interface\ConfigInterface;

final class ConfigTest extends TestCase
{
    private const CONFIG_FILE = 'config.test.yml';
    private const CONFIG_FILE_ERROR = 'config.test_error.yml';

    private function getConfigFile(string $configFile = self::CONFIG_FILE): string
    {
        return dirname(__FILE__).'/'.$configFile;
    }

    private function getConfig(
        ?string $env = null,
        ?CacheBaseInterface $cache = null,
        bool $flush = true): ConfigInterface
    {
        $configCache = null;
        if ($cache instanceof CacheBaseInterface) {
            $configCache = new ConfigCache($cache);
            if ($flush) {
                $configCache->getCache()->flush();
            }
        }

        return new Config($this->getConfigFile(), $env, $configCache);
    }

    private function getRedisHost(): string
    {
        $testingHost = getenv('REDIS_HOST');
        if (!is_string($testingHost)) {
            $testingHost = RedisCacheInterface::DEFAULT_HOST;
        }

        return $testingHost;
    }

    public function testGetConfigFile(): void
    {
        $this->assertSame($this->getConfigFile(), $this->getConfig()->getConfigFile());
    }

    public function testGetEnv(): void
    {
        $this->assertNull($this->getConfig()->getEnv());
        $this->assertSame('dev', $this->getConfig('dev')->getEnv());
    }

    public function testGetConfigCache(): void
    {
        $this->assertNull($this->getConfig()->getConfigCache());

        $config = $this->getConfig(null, new RedisCache($this->getRedisHost()));
        $this->assertInstanceOf(ConfigCacheInterface::class, $config->getConfigCache());
    }

    public function testGetCacheInterface(): void
    {
        $this->assertNull($this->getConfig()->getCacheInterface());

        $config = $this->getConfig(null, new RedisCache($this->getRedisHost()));
        $this->assertInstanceOf(CacheBaseInterface::class, $config->getCacheInterface());
    }

    public function testGetValues(): void
    {
        $config = $this->getConfig();

        // Aucun environnement ciblé :

        $this->assertInstanceOf(ParametersInterface::class, $config->get('entry1'));
        $this->assertSame(1, $config->get('entry1')->get('test1'));
        $this->assertSame(2, $config->get('entry1')->get('test2'));
        $this->assertSame('300', $config->get('entry1')->get('test3')->get('test3_1'));
        $this->assertSame(300, $config->get('entry1')->get('test3')->getInt('test3_1'));
        $this->assertSame('test3_2', $config->get('entry1')->get('test3')->get('test3_2'));

        $this->assertInstanceOf(ParametersInterface::class, $config->get('entry2'));
        $this->assertSame('test1', $config->get('entry2')->get('test1'));
        $this->assertSame(2, $config->get('entry2')->get('test2'));
        $this->assertSame(300, $config->get('entry2')->get('test3')->get('test3_1'));
        $this->assertSame('test3_2', $config->get('entry2')->get('test3')->get('test3_2'));

        // Environnement "dev" :

        $config = $this->getConfig('dev');

        $this->assertInstanceOf(ParametersInterface::class, $config->get('entry1'));
        $this->assertSame(1, $config->get('entry1')->get('test1'));
        $this->assertSame(22, $config->get('entry1')->get('test2'));
        $this->assertSame('300', $config->get('entry1')->get('test3')->get('test3_1'));
        $this->assertSame(300, $config->get('entry1')->get('test3')->getInt('test3_1'));
        $this->assertSame('test3_2_dev', $config->get('entry1')->get('test3')->get('test3_2'));

        $this->assertInstanceOf(ParametersInterface::class, $config->get('entry2'));
        $this->assertSame('test1', $config->get('entry2')->get('test1'));
        $this->assertSame('2', $config->get('entry2')->get('test2'));
        $this->assertSame(300, $config->get('entry2')->get('test3')->get('test3_1'));
        $this->assertSame('test2_test3_2_dev', $config->get('entry2')->get('test3')->get('test3_2'));
    }

    public function testPropsValues(): void
    {
        $config = $this->getConfig();

        $this->assertInstanceOf(ParametersInterface::class, $config->entry1);
        $this->assertSame(1, $config->entry1->test1);
        $this->assertSame(2, $config->entry1->test2);
        $this->assertSame('300', $config->entry1->test3->test3_1);
        $this->assertSame('test3_2', $config->entry1->test3->test3_2);

        $this->assertInstanceOf(ParametersInterface::class, $config->entry2);
        $this->assertSame('test1', $config->entry2->test1);
        $this->assertSame(2, $config->entry2->test2);
        $this->assertSame(300, $config->entry2->test3->test3_1);
        $this->assertSame('test3_2', $config->entry2->test3->test3_2);

        // Environnement "dev" :

        $config = $this->getConfig('dev');

        $this->assertInstanceOf(ParametersInterface::class, $config->entry1);
        $this->assertSame(1, $config->entry1->test1);
        $this->assertSame(22, $config->entry1->test2);
        $this->assertSame('300', $config->entry1->test3->test3_1);
        $this->assertSame('test3_2_dev', $config->entry1->test3->test3_2);

        $this->assertInstanceOf(ParametersInterface::class, $config->entry2);
        $this->assertSame('test1', $config->entry2->test1);
        $this->assertSame('2', $config->entry2->test2);
        $this->assertSame(300, $config->entry2->test3->test3_1);
        $this->assertSame('test2_test3_2_dev', $config->entry2->test3->test3_2);
    }

    private function testRedisCacheCommon(ConfigInterface $config): void
    {
        $cache = $config->getCacheInterface();
        $this->assertInstanceOf(RedisCacheInterface::class, $config->getCacheInterface());
        $this->assertIsString($cache->{ConfigCacheInterface::DEFAULT_CACHE_KEY});
    }

    public function testRedisCache(): void
    {
        $redisCache = new RedisCache($this->getRedisHost());

        $config = $this->getConfig(null, $redisCache);
        $this->testRedisCacheCommon($config);

        $config = $this->getConfig(null, $redisCache, false);
        $this->testRedisCacheCommon($config);

        /** @var RedisCacheInterface $cache */
        $cache = $config->getCacheInterface();
        $this->assertInstanceOf(RedisCacheInterface::class, $cache);

        $cache->flush();
    }

    private function testFileCacheCommon(ConfigInterface $config): void
    {
        $cache = $config->getCacheInterface();
        $this->assertInstanceOf(FileCacheInterface::class, $cache);
        $this->assertIsString($cache->{ConfigCacheInterface::DEFAULT_CACHE_KEY});
    }

    public function testFileCache(): void
    {
        $fileCache = new FileCache(dirname(__FILE__).'/cache');

        $config = $this->getConfig(null, $fileCache);
        $this->testFileCacheCommon($config);

        $config = $this->getConfig(null, $fileCache, false);
        $this->testFileCacheCommon($config);

        /** @var FileCacheInterface $cache */
        $cache = $config->getCacheInterface();
        $this->assertInstanceOf(FileCacheInterface::class, $cache);

        $cache->deferredPurge();
    }

    public function testError(): void
    {
        $this->expectException(ConfigException::class);

        new Config($this->getConfigFile(self::CONFIG_FILE_ERROR));
    }
}
